package io

import (
	"encoding/csv"
	"lollearn/classifiers"
	"os"
	"strconv"
	"strings"
)

// Writes ChampStats to CSV output.
type ChampStatsCSVWriter struct {
	OutFile *os.File
	Writer  *csv.Writer
}

// Provides the header for the CSV file.
func header() []string {
	return []string{
		"ID",
		"Name",
		"PassiveClassifications",
		"Spell1Classifications",
		"Spell2Classifications",
		"Spell3Classifications",
		"Spell4Classifications",
		"BuffScore",
		"DebuffScore",
		"CrowdControlScore",
		"AllyHealScore",
		"SelfHealScore",
		"MobilityScore",
		"AreaOfEffectScore",
		"AttackScore",
		"DefenseScore",
		"DifficultyScore",
		"MagicScore",
	}
}

// Converts ChampStats to CSV line.
func champStatsToCSVLine(stats *classifiers.ChampionStats) []string {
	return []string{
		strconv.Itoa(stats.ID),
		stats.Name,
		strings.Join(stats.PassiveClassifications, ";"),
		strings.Join(stats.Spell1Classifications, ";"),
		strings.Join(stats.Spell2Classifications, ";"),
		strings.Join(stats.Spell3Classifications, ";"),
		strings.Join(stats.Spell4Classifications, ";"),
		strconv.Itoa(stats.BuffScore),
		strconv.Itoa(stats.DebuffScore),
		strconv.Itoa(stats.CrowdControlScore),
		strconv.Itoa(stats.AllyHealScore),
		strconv.Itoa(stats.SelfHealScore),
		strconv.Itoa(stats.MobilityScore),
		strconv.Itoa(stats.AreaOfEffectScore),
		strconv.Itoa(stats.AttackScore),
		strconv.Itoa(stats.DefenseScore),
		strconv.Itoa(stats.DifficultyScore),
		strconv.Itoa(stats.MagicScore),
	}
}

// Create new ChampStatsCSVWriter instace.
func NewChampStatsCSVWriter(outputPath string) (*ChampStatsCSVWriter, error) {
	w := new(ChampStatsCSVWriter)
	// write out champs to csv
	outFile, err := os.Create(outputPath)
	if err != nil {
		return nil, err
	}

	w.OutFile = outFile
	w.Writer = csv.NewWriter(w.OutFile)
	w.Writer.Write(header())

	return w, err
}

// Writes ChampStats to output file.
func (w *ChampStatsCSVWriter) Write(stats *classifiers.ChampionStats) {
	w.Writer.Write(champStatsToCSVLine(stats))
}

// Closes the output file.
func (w *ChampStatsCSVWriter) Close() {
	w.Writer.Flush()
	w.OutFile.Close()
}
