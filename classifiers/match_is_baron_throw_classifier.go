package classifiers

import (
    "fmt"

	"lolapi"
)

// Center x/y values of baron nashor
const BARON_X int64 = 5007
const BARON_Y int64 = 10471

// Radius around baron nashor considered the "baron zone"
const BARON_R int64 = 1947

// Seconds to keep track of kill events before baron was killed
const BEFORE_THRESHOLD int64 = 30

// Seconds to keep track of kill events after baron was killed
const AFTER_THRESHOLD int64 = 30

// Container for a baron event and associated kill events within the before
// and after thresholds
type BaronEvent struct {
	FrameIndex int
	Timestamp  int64
	KillerID   int64
	KillEvents []KillEvent
}

// Keeps track of who killed who, where they were killed and the time they died
type KillEvent struct {
	Timestamp int64
	KillerID  int64
	VictimID  int64
	XCoord    int64
	YCoord    int64
}

// Determine what team the parcipant is on. Right now the API has a scale of
// 1 - 10 for IDs. 1-5 = blue, 6-10 = red
func teamForParticipantID(x int64) string {
	if x >= 1 && x <= 5 {
		return "blue"
	}

	if x == 0 {
		return ""
	}

	return "red"
}

// Provides new instance of BaronEvent
func NewBaronEvent() *BaronEvent {
	be := new(BaronEvent)
	be.KillEvents = make([]KillEvent, 0)
	return be
}

// Adds a KillEvent
func (be *BaronEvent) AddKillEvent(k KillEvent) {
	be.KillEvents = append(be.KillEvents, k)
}

// Helper to see if a x,y coord exists within the area of the baron zone
func killInBaronZone(x, y int64) bool {
	dx := BARON_X - x
	dy := BARON_Y - y
	squareDist := (dx * dx) + (dy * dy)
	squareR := BARON_R * BARON_R
	return squareDist <= squareR
}

// Convert milliseconds to minutes
func msToM(ms int64) int64 {
	return ms / 1000 / 60
}

// Convert milliseconds to seconds
func msToS(ms int64) int64 {
	return ms / 1000
}

// Check if match contains any baron kills
func baronKilled(match lolapi.MatchDetailDto) bool {
	return match.Teams[0].BaronKills > 0 ||
		match.Teams[1].BaronKills > 0
}

// Check if match has any baron throws in it
func MatchIsBaronThrow(match lolapi.MatchDetailDto, summonerId string) bool {
	if !baronKilled(match) {
		return false
	}
	// find the summoner's team
	summonerTeam := ""
	for _, p := range match.ParticipantIdentities {
		testSummonerId := fmt.Sprintf("%d", p.Player.SummonerID)
		if testSummonerId == summonerId {
			summonerTeam = teamForParticipantID(p.ParticipantID)
			break
		}
	}

	// Find and keep track of all baron events.
	baronEvents := make([]*BaronEvent, 0)
	for i, f := range match.Timeline.Frames {
		// Baron doesn't spawn until 20 minutes in game so we can skip events
		// earlier events.
		minute := msToM(f.Timestamp)
		if minute < 20 {
			continue
		}

		for _, e := range f.Events {
			if e.MonsterType == "BARON_NASHOR" {
				be := NewBaronEvent()
				be.FrameIndex = i
				be.Timestamp = e.Timestamp
				be.KillerID = e.KillerID
				baronEvents = append(baronEvents, be)
			}
		}
	}

	// Loop over found baron events and find kill events within given time thresholds.
	// We store the kill events found within the threshold.
	for _, be := range baronEvents {

		// Figure out whihc frame index to start and stop at.
		// Each frame specifies a minute interval in time.
		// We only care about events within a short interval of the baron
		// so we only start at one frame before and after.
		startFrameIndex := be.FrameIndex - 1
		endFrameIndex := be.FrameIndex + 1
		if endFrameIndex > len(match.Timeline.Frames) {
			endFrameIndex = len(match.Timeline.Frames) - 1
		}

		for _, f := range match.Timeline.Frames[startFrameIndex:endFrameIndex] {
			for _, e := range f.Events {
				if e.EventType == "CHAMPION_KILL" {
					if e.Timestamp < be.Timestamp {
						secsBefore := msToS(be.Timestamp - e.Timestamp)
						if secsBefore <= BEFORE_THRESHOLD {
							be.AddKillEvent(KillEvent{e.Timestamp, e.KillerID, e.VictimID, e.Position.X, e.Position.Y})
						}
					} else {
						secsAfter := msToS(e.Timestamp - be.Timestamp)
						if secsAfter <= AFTER_THRESHOLD {
							be.AddKillEvent(KillEvent{e.Timestamp, e.KillerID, e.VictimID, e.Position.X, e.Position.Y})
						}
					}
				}
			}
		}
	}

	// Determine if any baron events has "near aces" or aces in them within the
	// baron zone.
	// Essentially - 4 or 5 deaths on either team.
	isThrow := false
	for _, be := range baronEvents {
		blueDeaths := 0
		redDeaths := 0
		for _, ke := range be.KillEvents {
			if killInBaronZone(ke.XCoord, ke.YCoord) {
				victim := teamForParticipantID(ke.VictimID)
				if victim == "blue" {
					blueDeaths++
				} else {
					redDeaths++
				}
			}
		}

		if (summonerTeam == "blue" && blueDeaths >= 4) ||
			(summonerTeam == "red" && redDeaths >= 4) {
			isThrow = true
			break
		}
	}

	return isThrow
}
