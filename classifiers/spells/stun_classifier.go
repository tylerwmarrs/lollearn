package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability is a stun.
type StunClassifier struct{}

// Label for this classifier
func (c *StunClassifier) GetLabel() string {
	return "Crowd Control Stun"
}

// Classifies the given text to test if it meets this
// classification of Stun.
func (c *StunClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"stun", "enemy"}, minedText)
}
