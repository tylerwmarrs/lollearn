package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability is a knock for enemies.
type KnockClassifier struct{}

// Label for this classifier
func (c *KnockClassifier) GetLabel() string {
	return "Crowd Control Knock"
}

// Classifies the given text to test if it meets this
// classification of Knock.
func (c *KnockClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.IsBefore("knock", "enemy", minedText) ||
		nlp.IsBefore("enemy", "knock", minedText)
}
