package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability is a reduction of armor for enemies.
type ArmorDebuffClassifier struct{}

// Label for this classifier
func (c *ArmorDebuffClassifier) GetLabel() string {
	return "Armor Debuff"
}

// Classifies the given text to test if it meets this
// classification of ArmorDebuff.
func (c *ArmorDebuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"reduce", "armor"}, minedText)
}
