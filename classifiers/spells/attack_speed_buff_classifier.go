package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability increases attack speed or not.
type AttackSpeedBuffClassifier struct{}

// Label for this classifier
func (c *AttackSpeedBuffClassifier) GetLabel() string {
	return "Attack Speed Buff"
}

// Classifies the given text to test if it meets this
// classification of Movement Speed Increase.
func (c *AttackSpeedBuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"increase", "attack", "speed"}, minedText) ||
		nlp.AppearInOrder([]string{"attack", "speed", "increase"}, minedText)
}
