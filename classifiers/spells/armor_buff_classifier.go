package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability increases armor or not.
type ArmorBuffClassifier struct{}

// Label for this classifier
func (c *ArmorBuffClassifier) GetLabel() string {
	return "Armor Increase"
}

// Classifies the given text to test if it meets this
// classification of ArmorBuff.
func (c *ArmorBuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.IsBefore("increase", "armor", minedText) ||
		nlp.IsBefore("armor", "increase", minedText)
}
