package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability is a blind for enemies.
type BlindClassifier struct{}

// Label for this classifier
func (c *BlindClassifier) GetLabel() string {
	return "Crowd Control Blind"
}

// Classifies the given text to test if it meets this
// classification of Blind.
func (c *BlindClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"blind", "enemy"}, minedText) ||
		nlp.AppearInOrder([]string{"reduce", "enemy", "vision"}, minedText) ||
		nlp.AppearInOrder([]string{"reduce", "sight"}, minedText)
}
