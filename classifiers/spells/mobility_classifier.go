package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability increases mobility or not.
type MobilityClassifier struct{}

// Label for this classifier
func (c *MobilityClassifier) GetLabel() string {
	return "Mobility"
}

// Classifies the given text to test if it meets this
// classification of Mobility.
func (c *MobilityClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.ContainsAny([]string{"leap", "jump", "hop",
		"dash", "teleport", "travel"}, minedText) ||
		nlp.AppearInOrder([]string{"carry", "forward"}, minedText)
}
