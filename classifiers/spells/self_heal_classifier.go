package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability is self healing or not.
type SelfHealClassifier struct{}

// Label for this classifier
func (c *SelfHealClassifier) GetLabel() string {
	return "Self Heal"
}

// Classifies the given text to test if it meets this
// classification of SelfHeal.
func (c *SelfHealClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.IsBefore("self", "heal", minedText)
}
