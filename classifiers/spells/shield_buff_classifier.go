package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability provides a shield or not.
type ShieldBuffClassifier struct{}

// Label for this classifier
func (c *ShieldBuffClassifier) GetLabel() string {
	return "Shield Buff"
}

// Classifies the given text to test if it meets this
// classification of Shield Buff.
func (c *ShieldBuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"shield", "ally"}, minedText) ||
		nlp.AppearInOrder([]string{"self", "shield"}, minedText) ||
		nlp.AppearInOrder([]string{"shield", "self"}, minedText)
}
