package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability is a slow for enemies.
type SlowClassifier struct{}

// Label for this classifier
func (c *SlowClassifier) GetLabel() string {
	return "Crowd Control Slow"
}

// Classifies the given text to test if it meets this
// classification of Slow.
func (c *SlowClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.IsBefore("slow", "enemy", minedText) ||
		nlp.AppearInOrder([]string{"steal", "movement", "speed"}, minedText)
}
