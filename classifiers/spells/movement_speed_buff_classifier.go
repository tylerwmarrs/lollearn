package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability increases movement speed or not.
type MovementSpeedBuffClassifier struct{}

// Label for this classifier
func (c *MovementSpeedBuffClassifier) GetLabel() string {
	return "Movement Speed Buff"
}

// Classifies the given text to test if it meets this
// classification of Movement Speed Buff.
func (c *MovementSpeedBuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"increase", "movement", "speed"}, minedText) ||
		nlp.AppearInOrder([]string{"movement", "speed", "increase"}, minedText)
}
