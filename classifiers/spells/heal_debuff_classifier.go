package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability is a reduction of healing for enemies.
type HealDebuffClassifier struct{}

// Label for this classifier
func (c *HealDebuffClassifier) GetLabel() string {
	return "Heal Debuff"
}

// Classifies the given text to test if it meets this
// classification of HealDebuff.
func (c *HealDebuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.IsBefore("reduce", "heal", minedText)
}
