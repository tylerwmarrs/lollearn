package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability increases magic resist or not.
type MagicResistBuffClassifier struct{}

// Label for this classifier
func (c *MagicResistBuffClassifier) GetLabel() string {
	return "Magic Resist Increase"
}

// Classifies the given text to test if it meets this
// classification of MagicResistBuff.
func (c *MagicResistBuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"magic", "resist", "increase"}, minedText) ||
		nlp.AppearInOrder([]string{"increase", "magic", "resist"}, minedText)
}
