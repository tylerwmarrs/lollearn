package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability is area of effect or not.
type AreaOfEffectClassifier struct{}

// Label for this classifier
func (c *AreaOfEffectClassifier) GetLabel() string {
	return "Area of Effect"
}

// Classifies the given text to test if it meets this
// classification of AreaOfEffect.
func (c *AreaOfEffectClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.ContainsAny([]string{"surround", "close", "around", "ground",
		"area"}, minedText)
}
