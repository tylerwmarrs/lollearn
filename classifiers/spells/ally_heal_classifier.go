package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability is ally healing or not.
type AllyHealClassifier struct{}

// Label for this classifier
func (c *AllyHealClassifier) GetLabel() string {
	return "Ally Heal"
}

// Classifies the given text to test if it meets this
// classification of SelfHeal.
func (c *AllyHealClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.ContainsAll([]string{"ally", "heal"}, minedText) ||
		nlp.IsBefore("target", "heal", minedText) ||
		nlp.AppearInOrder([]string{"restore", "health", "ally"}, minedText)
}
