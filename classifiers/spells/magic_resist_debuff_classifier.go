package spells

import (
    "go-nlp"
)

// A boolean classifier that determines if a champions
// ability is a reduction of magic resist for enemies.
type MagicResistDebuffClassifier struct{}

// Label for this classifier
func (c *MagicResistDebuffClassifier) GetLabel() string {
	return "Magic Resist Debuff"
}

// Classifies the given text to test if it meets this
// classification of MagicResistDebuff.
func (c *MagicResistDebuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"magic", "resist", "reduce"}, minedText)
}
