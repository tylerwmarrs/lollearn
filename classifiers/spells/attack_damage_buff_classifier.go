package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability provides a attack damage buff or not.
type AttackDamageBuffClassifier struct{}

// Label for this classifier
func (c *AttackDamageBuffClassifier) GetLabel() string {
	return "Attack Damage Buff"
}

// Classifies the given text to test if it meets this
// classification of attack Damage Buff.
func (c *AttackDamageBuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"increase", "attack", "damage"}, minedText) ||
		nlp.AppearInOrder([]string{"attack", "damage", "increase"}, minedText) ||
		nlp.AppearInOrder([]string{"self", "damage", "increase"}, minedText)
}
