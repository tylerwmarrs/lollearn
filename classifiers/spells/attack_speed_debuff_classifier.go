package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability is a reduction of attack speed for enemies.
type AttackSpeedDebuffClassifier struct{}

// Label for this classifier
func (c *AttackSpeedDebuffClassifier) GetLabel() string {
	return "Attack Speed Debuff"
}

// Classifies the given text to test if it meets this
// classification of ArmorDebuff.
func (c *AttackSpeedDebuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"attack", "speed", "reduce"}, minedText)
}
