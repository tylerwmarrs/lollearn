package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability provides a magic damage buff or not.
type MagicDamageBuffClassifier struct{}

// Label for this classifier
func (c *MagicDamageBuffClassifier) GetLabel() string {
	return "Magic Damage Buff"
}

// Classifies the given text to test if it meets this
// classification of Magic Damage Buff.
func (c *MagicDamageBuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"increase", "magic", "damage"}, minedText) ||
		nlp.AppearInOrder([]string{"magic", "damage", "increase"}, minedText) ||
		nlp.AppearInOrder([]string{"magic", "penetration", "increase"}, minedText) ||
		nlp.AppearInOrder([]string{"increase", "magic", "penetration"}, minedText)
}
