package spells

import (
	"go-nlp"
)

// A boolean classifier that determines if a champions
// ability immune buff or not.
type ImmuneBuffClassifier struct{}

// Label for this classifier
func (c *ImmuneBuffClassifier) GetLabel() string {
	return "Immune Buff"
}

// Classifies the given text to test if it meets this
// classification of immune Buff.
func (c *ImmuneBuffClassifier) Classify(minedText nlp.MinedResults) bool {
	return nlp.AppearInOrder([]string{"immune", "damage"}, minedText) ||
		nlp.ContainsAny([]string{"invulnerable", "untargetable"}, minedText)
}
