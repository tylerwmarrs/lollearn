package classifiers

import (
	"bufio"
	"lolapi"
	"lollearn/classifiers/spells"
	"go-nlp"
	"os"
	"strings"
)

var lemmatizer *nlp.Lemmatizer
var miner *nlp.KeywordTextMiner

// All buff classifiers to test
var bc []BooleanClassifier

// All debuff classifiers to test
var db []BooleanClassifier

// All cc classifiers to test
var cc []BooleanClassifier

// AoE classifier
var aoec BooleanClassifier

// Self heal classifier
var shc BooleanClassifier

// Ally heal classifier
var ahc BooleanClassifier

// Mobility classifier
var mc BooleanClassifier

// Contains precomputed values for champion
// spell classifications and other attributes.
type ChampionStats struct {
	// ID of champion
	ID int
	// Champion's name
	Name string
	// Classifications for the champions passive.
	PassiveClassifications []string
	// Classifications for the champions Q.
	Spell1Classifications []string
	// Classifications for the champions W.
	Spell2Classifications []string
	// Classifications for the champions E.
	Spell3Classifications []string
	// Classifications for the champions R.
	Spell4Classifications []string
	// Total number of spells considered to be buffs.
	BuffScore int
	// Total number of spells considered to be debuffs.
	DebuffScore int
	// Total number of spells considered to be cc.
	CrowdControlScore int
	// Total number of spells considered to be ally heals.
	AllyHealScore int
	// Total number of spells considered to be self heals.
	SelfHealScore int
	// Total number of spells considered to be mobility.
	MobilityScore int
	// Total number of spells considered to be AoE.
	AreaOfEffectScore int
	// Attack stat from ChampionInfoDto.
	AttackScore int
	// Defense stat from ChampionInfoDto.
	DefenseScore int
	// Difficulty stat from ChampionInfoDto.
	DifficultyScore int
	// Magic stat from ChampionInfoDto.
	MagicScore                int
	ArmorScore                float32
	ArmorPerLevelScore        float32
	AttackDamageScore         float32
	AttackDamagePerLevelScore float32
	AttackRangeScore          float32
	AttackSpeedOffsetScore    float32
	AttackSpeedPerLevelScore  float32
	CritScore                 float32
	CritPerLevelScore         float32
	HpScore                   float32
	HpPerLevelScore           float32
	HpRegenScore              float32
	HpRegenPerLevelScore      float32
	MoveSpeedScore            float32
	MpScore                   float32
	MpPerLevelScore           float32
	MpRegenScore              float32
	MpRegenPerLevelScore      float32
	SpellBlockScore           float32
	SpellBlockPerLevelScore   float32
}

// Helper to create new instance of ChampionStats.
// It initializes slices etc.
func newChampionStats() *ChampionStats {
	c := new(ChampionStats)
	c.PassiveClassifications = make([]string, 0)
	c.Spell1Classifications = make([]string, 0)
	c.Spell2Classifications = make([]string, 0)
	c.Spell3Classifications = make([]string, 0)
	c.Spell4Classifications = make([]string, 0)

	return c
}

func classifyText(stats *ChampionStats,
	champion *lolapi.ChampionDto,
	text string,
	appender func(s *ChampionStats, d string)) {

	tmp := strings.Replace(text, champion.Name, "self", -1)
	data := miner.MineText(tmp)

	// classify buffs
	for _, c := range bc {
		if c.Classify(data) {
			appender(stats, c.GetLabel())
			stats.BuffScore++
		}
	}

	// classify debuffs
	for _, c := range db {
		if c.Classify(data) {
			appender(stats, c.GetLabel())
			stats.DebuffScore++
		}
	}

	// classify crowd control
	for _, c := range cc {
		if c.Classify(data) {
			appender(stats, c.GetLabel())
			stats.CrowdControlScore++
		}
	}

	// classify area of offect
	if aoec.Classify(data) {
		appender(stats, aoec.GetLabel())
		stats.AreaOfEffectScore++
	}

	// classify self heal
	if shc.Classify(data) {
		appender(stats, shc.GetLabel())
		stats.SelfHealScore++
	}

	// classify ally heal
	if ahc.Classify(data) {
		appender(stats, ahc.GetLabel())
		stats.AllyHealScore++
	}

	// classify mobility
	if mc.Classify(data) {
		appender(stats, mc.GetLabel())
		stats.MobilityScore++
	}
}

func classifyPassive(stats *ChampionStats, champion *lolapi.ChampionDto) {
	appender := func(s *ChampionStats, d string) {
		s.PassiveClassifications = append(s.PassiveClassifications, d)
	}
	classifyText(stats, champion, champion.Passive.Description, appender)
}

func classifySpell1(stats *ChampionStats, champion *lolapi.ChampionDto) {
	appender := func(s *ChampionStats, d string) {
		s.Spell1Classifications = append(s.Spell1Classifications, d)
	}
	classifyText(stats, champion, champion.Spells[0].Description, appender)
}

func classifySpell2(stats *ChampionStats, champion *lolapi.ChampionDto) {
	appender := func(s *ChampionStats, d string) {
		s.Spell2Classifications = append(s.Spell2Classifications, d)
	}
	classifyText(stats, champion, champion.Spells[1].Description, appender)
}

func classifySpell3(stats *ChampionStats, champion *lolapi.ChampionDto) {
	appender := func(s *ChampionStats, d string) {
		s.Spell3Classifications = append(s.Spell3Classifications, d)
	}
	classifyText(stats, champion, champion.Spells[2].Description, appender)
}

func classifySpell4(stats *ChampionStats, champion *lolapi.ChampionDto) {
	appender := func(s *ChampionStats, d string) {
		s.Spell4Classifications = append(s.Spell4Classifications, d)
	}
	classifyText(stats, champion, champion.Spells[3].Description, appender)
}

func ClassifyChampion(champion *lolapi.ChampionDto) *ChampionStats {
	result := newChampionStats()
	result.ID = champion.ID
	result.Name = champion.Name

	// add basic stats - attack, magic etc.
	result.AttackScore = champion.Info.Attack
	result.DefenseScore = champion.Info.Defense
	result.DifficultyScore = champion.Info.Difficulty
	result.MagicScore = champion.Info.Magic
	result.ArmorScore = champion.Stats.Armor
	result.ArmorPerLevelScore = champion.Stats.ArmorPerLevel
	result.AttackDamageScore = champion.Stats.AttackDamage
	result.AttackDamagePerLevelScore = champion.Stats.AttackDamagePerLevel
	result.AttackRangeScore = champion.Stats.AttackRange
	result.AttackSpeedOffsetScore = champion.Stats.AttackSpeedOffset
	result.AttackSpeedPerLevelScore = champion.Stats.AttackSpeedPerLevel
	result.CritScore = champion.Stats.Crit
	result.CritPerLevelScore = champion.Stats.CritPerLevel
	result.HpScore = champion.Stats.Hp
	result.HpPerLevelScore = champion.Stats.HpPerLevel
	result.HpRegenScore = champion.Stats.HpRegen
	result.HpRegenPerLevelScore = champion.Stats.HpRegenPerLevel
	result.MoveSpeedScore = champion.Stats.MoveSpeed
	result.MpScore = champion.Stats.Mp
	result.MpPerLevelScore = champion.Stats.MpPerLevel
	result.MpRegenScore = champion.Stats.MpRegen
	result.MpRegenPerLevelScore = champion.Stats.MpRegenPerLevel
	result.SpellBlockScore = champion.Stats.SpellBlock
	result.SpellBlockPerLevelScore = champion.Stats.SpellBlockPerLevel

	classifyPassive(result, champion)
	classifySpell1(result, champion)
	classifySpell2(result, champion)
	classifySpell3(result, champion)
	classifySpell4(result, champion)

	return result
}

func init() {
	// create lemmatizer with lemmas
	lemmatizer = nlp.NewLemmatizer()
	lemmatizer.AddLemma(nlp.NewLemma("stun", []string{"stunning", "stunned", "stuns", "freeze", "freezes"}))
	lemmatizer.AddLemma(nlp.NewLemma("surround", []string{"surrounding", "surrounds"}))
	lemmatizer.AddLemma(nlp.NewLemma("slow", []string{"slows", "slowing"}))
	lemmatizer.AddLemma(nlp.NewLemma("leap", []string{"leaps", "leaping", "leaped"}))
	lemmatizer.AddLemma(nlp.NewLemma("jump", []string{"jumping", "jumped", "jumps"}))
	lemmatizer.AddLemma(nlp.NewLemma("hop", []string{"hopping", "hops", "hopped"}))
	lemmatizer.AddLemma(nlp.NewLemma("channel", []string{"channels", "channeling", "channelled"}))
	lemmatizer.AddLemma(nlp.NewLemma("knock", []string{"knocks", "knocking", "knocked"}))
	lemmatizer.AddLemma(nlp.NewLemma("dash", []string{"dashes", "dashing", "dashed"}))
	lemmatizer.AddLemma(nlp.NewLemma("reduce", []string{"reduces", "reduced", "reducing", "reduction"}))
	lemmatizer.AddLemma(nlp.NewLemma("heal", []string{"heals", "healed", "healing"}))
	lemmatizer.AddLemma(nlp.NewLemma("resist", []string{"resistance", "resists", "resisting"}))
	lemmatizer.AddLemma(nlp.NewLemma("protect", []string{"protects", "protected", "protecting"}))
	lemmatizer.AddLemma(nlp.NewLemma("area", []string{"near", "nearby", "nearest", "close"}))
	lemmatizer.AddLemma(nlp.NewLemma("miss", []string{"missing", "missed", "misses"}))
	lemmatizer.AddLemma(nlp.NewLemma("speed", []string{"speeds", "speeding"}))
	lemmatizer.AddLemma(nlp.NewLemma("increase", []string{"increases", "increasing", "increased"}))
	lemmatizer.AddLemma(nlp.NewLemma("damage", []string{"damages", "damaging", "damaged"}))
	lemmatizer.AddLemma(nlp.NewLemma("accelerate", []string{"accelerates", "accelerating", "accelerated"}))
	lemmatizer.AddLemma(nlp.NewLemma("gain", []string{"regain", "regaining", "regains"}))
	lemmatizer.AddLemma(nlp.NewLemma("teleport", []string{"teleports", "teleporting"}))
	lemmatizer.AddLemma(nlp.NewLemma("hit", []string{"hitting", "hits"}))
	lemmatizer.AddLemma(nlp.NewLemma("choose", []string{"chooses", "choosing"}))
	lemmatizer.AddLemma(nlp.NewLemma("reveal", []string{"revealing", "reveals", "revealed"}))
	lemmatizer.AddLemma(nlp.NewLemma("receive", []string{"receiving", "received", "receives"}))
	lemmatizer.AddLemma(nlp.NewLemma("travel", []string{"traveling", "travels", "traveled"}))
	lemmatizer.AddLemma(nlp.NewLemma("transform", []string{"transforms", "transforming", "transformed"}))
	lemmatizer.AddLemma(nlp.NewLemma("collide", []string{"collides", "colliding", "collided"}))
	lemmatizer.AddLemma(nlp.NewLemma("regenerate", []string{"regen", "regeneration", "regenerates"}))
	lemmatizer.AddLemma(nlp.NewLemma("blind", []string{"blinding", "blinded", "blinds"}))
	lemmatizer.AddLemma(nlp.NewLemma("silence", []string{"silences", "silencing", "silenced"}))
	lemmatizer.AddLemma(nlp.NewLemma("enemy", []string{"enemies"}))
	lemmatizer.AddLemma(nlp.NewLemma("shield", []string{"shields", "shielding"}))
	lemmatizer.AddLemma(nlp.NewLemma("carry", []string{"carries", "carrying"}))
	lemmatizer.AddLemma(nlp.NewLemma("ally", []string{"allies", "friendly"}))
	lemmatizer.AddLemma(nlp.NewLemma("restore", []string{"restoring", "restores"}))
	lemmatizer.AddLemma(nlp.NewLemma("steal", []string{"stealing", "steals"}))

	// create miner and keywords
	miner = nlp.NewKeywordTextMiner()
	miner.SetWordNormalizer(lemmatizer)
	miner.AddKeyword("health")
	miner.AddKeyword("heal")
	miner.AddKeyword("self")
	miner.AddKeyword("himself")
	miner.AddKeyword("herself")
	miner.AddKeyword("increase")
	miner.AddKeyword("movement")
	miner.AddKeyword("attack")
	miner.AddKeyword("speed")
	miner.AddKeyword("armor")
	miner.AddKeyword("magic")
	miner.AddKeyword("resist")
	miner.AddKeyword("shield")
	miner.AddKeyword("ally")
	miner.AddKeyword("damage")
	miner.AddKeyword("immune")
	miner.AddKeyword("untargetable")
	miner.AddKeyword("invulnerable")
	miner.AddKeyword("reduce")
	miner.AddKeyword("penetration")
	miner.AddKeyword("vision")
	miner.AddKeyword("blind")
	miner.AddKeyword("sight")
	miner.AddKeyword("stun")
	miner.AddKeyword("enemy")
	miner.AddKeyword("slow")
	miner.AddKeyword("knock")
	miner.AddKeyword("leap")
	miner.AddKeyword("jump")
	miner.AddKeyword("dash")
	miner.AddKeyword("hop")
	miner.AddKeyword("teleport")
	miner.AddKeyword("travel")
	miner.AddKeyword("carry")
	miner.AddKeyword("forward")
	miner.AddKeyword("surround")
	miner.AddKeyword("close")
	miner.AddKeyword("around")
	miner.AddKeyword("ground")
	miner.AddKeyword("area")
	miner.AddKeyword("restore")
	miner.AddKeyword("steal")

	// read and add stopwords to miner
	inFile, _ := os.Open("B:\\src\\go\\data\\stopwords.txt")
	defer inFile.Close()
	scanner := bufio.NewScanner(inFile)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		line := scanner.Text()
		if !strings.HasPrefix(line, "#") {
			miner.AddStopWord(line)
		}
	}

	// create classifiers
	bc = []BooleanClassifier{&spells.ArmorBuffClassifier{},
		&spells.AttackDamageBuffClassifier{},
		&spells.AttackSpeedBuffClassifier{},
		&spells.ImmuneBuffClassifier{},
		&spells.MagicDamageBuffClassifier{},
		&spells.MagicResistBuffClassifier{},
		&spells.MovementSpeedBuffClassifier{},
		&spells.ShieldBuffClassifier{}}

	db = []BooleanClassifier{&spells.ArmorDebuffClassifier{},
		&spells.AttackSpeedDebuffClassifier{},
		&spells.HealDebuffClassifier{},
		&spells.MagicResistDebuffClassifier{}}

	cc = []BooleanClassifier{&spells.BlindClassifier{},
		&spells.KnockClassifier{},
		&spells.SlowClassifier{},
		&spells.StunClassifier{}}

	aoec = new(spells.AreaOfEffectClassifier)
	shc = new(spells.SelfHealClassifier)
	ahc = new(spells.AllyHealClassifier)
	mc = new(spells.MobilityClassifier)
}
