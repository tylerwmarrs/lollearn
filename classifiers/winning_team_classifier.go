package classifiers

/*
=== Run information ===

Scheme:weka.classifiers.trees.J48 -R -N 5 -Q 1 -B -M 2 -A
Relation:     team_win_rates_masters2
Instances:    3304
Attributes:   3
              BlueTeam
              RedTeam
              Win
Test mode:user supplied test set:     641instances

=== Classifier model (full training set) ===

J48 pruned tree
------------------

RedTeam <= 285.871643
|   BlueTeam <= 253.400162
|   |   RedTeam <= 258.712128
|   |   |   BlueTeam <= 229.873032: RedTeam (33.0/5.0)
|   |   |   BlueTeam > 229.873032: BlueTeam (38.0/13.0)
|   |   RedTeam > 258.712128: RedTeam (146.0/10.0)
|   BlueTeam > 253.400162
|   |   RedTeam <= 245.268372: BlueTeam (556.0/6.0)
|   |   RedTeam > 245.268372
|   |   |   BlueTeam <= 302.380981
|   |   |   |   BlueTeam <= 265.680847: RedTeam (53.0/18.0)
|   |   |   |   BlueTeam > 265.680847: BlueTeam (262.0/90.0)
|   |   |   BlueTeam > 302.380981: BlueTeam (286.0/18.0)
RedTeam > 285.871643
|   BlueTeam <= 284.519928: RedTeam (954.0/50.0)
|   BlueTeam > 284.519928
|   |   RedTeam <= 316.890594: BlueTeam (196.0/62.0)
|   |   RedTeam > 316.890594
|   |   |   BlueTeam <= 324.080078: RedTeam (101.0/6.0)
|   |   |   BlueTeam > 324.080078
|   |   |   |   BlueTeam <= 344.303864
|   |   |   |   |   RedTeam <= 330.416656: BlueTeam (7.0/2.0)
|   |   |   |   |   RedTeam > 330.416656: RedTeam (7.0)
|   |   |   |   BlueTeam > 344.303864: BlueTeam (5.0)

Number of Leaves  : 	13

Size of the tree : 	25


Time taken to build model: 0.03 seconds

=== Evaluation on test set ===
=== Summary ===

Correctly Classified Instances         603               94.0718 %
Incorrectly Classified Instances        38                5.9282 %
Kappa statistic                          0.8814
Mean absolute error                      0.1043
Root mean squared error                  0.2121
Relative absolute error                 20.856  %
Root relative squared error             42.3594 %
Total Number of Instances              641

=== Detailed Accuracy By Class ===

               TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
                 0.956     0.075      0.927     0.956     0.942      0.975    BlueTeam
                 0.925     0.044      0.955     0.925     0.94       0.975    RedTeam
Weighted Avg.    0.941     0.059      0.941     0.941     0.941      0.975

=== Confusion Matrix ===

   a   b   <-- classified as
 306  14 |   a = BlueTeam
  24 297 |   b = RedTeam
*/

// This classifies which league of legends team will win based on the
// summation of each player's specific champion win rate for their team.
// This is the implementation of the J48 decision tree's output. Overall this
// classifier has an accuracy of rougly 92%.
// The output of this classifier is a string of "blue" or "red".
func CWRWinningTeamClassifier(blueWinRate float64, redWinRate float64) string {
	var winner string
	if redWinRate <= 285.871643 {
		if blueWinRate <= 253.400162 {
			if redWinRate <= 258.712128 {
				if blueWinRate <= 229.873032 {
					winner = "red"
				} else {
					winner = "blue"
				}
			} else {
				winner = "red"
			}
		} else {
			if redWinRate <= 245.268372 {
				winner = "blue"
			} else {
				if blueWinRate <= 302.380981 {
					if blueWinRate <= 265.680847 {
						winner = "red"
					} else {
						winner = "blue"
					}
				} else {
					winner = "blue"
				}
			}
		}
	} else {
		if blueWinRate <= 284.519928 {
			winner = "red"
		} else {
			if redWinRate <= 316.890594 {
				winner = "blue"
			} else {
				if blueWinRate <= 324.080078 {
					winner = "red"
				} else {
					if blueWinRate <= 344.303864 {
						if redWinRate <= 330.416656 {
							winner = "blue"
						} else {
							winner = "red"
						}
					} else {
						winner = "blue"
					}
				}
			}
		}
	}
	return winner
}
