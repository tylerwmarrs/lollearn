// This package contains classification logic for many different
// aspects of the game.
package classifiers

import (
	"go-nlp"
)

// Simple classifier that simply determines if the classification
// is met or not.
type BooleanClassifier interface {
	// The classification label.
	GetLabel() string
	// Whether or not classification was met.
	Classify(minedText nlp.MinedResults) bool
}
